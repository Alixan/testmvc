<?php

ini_set('display_errors', 'On');
error_reporting(E_ALL|E_NOTICE);

require __DIR__.'/Controller/HomeController.php';

$uri = $_SERVER['REQUEST_URI'];

$route = (new \App\Request\Request())->parseRequest($uri);

switch ($route) {
    case '/':
        $controller = new \App\Controller\HomeController();
        return $controller->run();
        break;
}