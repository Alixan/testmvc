<?php

namespace App\Controller;

/**
 * Class BaseController
 */
abstract class BaseController
{
    public function render($viewName, $params=[])
    {
        $content = $this->renderFile($viewName, $params);

        // require layouts main
    }

    public function renderLayouts($viewPartial)
    {
        $content = $viewPartial;

        // подрубить лайоутс и передать переменную контент
    }

    public function renderFile($viewName, $params)
    {

    }
}