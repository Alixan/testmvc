<?php

namespace App\Controller;

require __DIR__.'/BaseController.php';

/**
 * Class HomeController
 */
class HomeController extends BaseController
{
    public function run()
    {
        // Обращение к модели - получение данные
        // Передача данных в шаблон

        return $this->render('index', []);
    }
}